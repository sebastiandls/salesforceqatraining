import Page from './page'

class HomePage extends Page {

  get pageTitle ()   { return browser.element('h1') }
  get links ()       { return browser.elements('ul li a') }

  open () {
    super.open('/')
    this.pageTitle.waitForVisible()
  }

  clickLink(text) {
    this.links.waitForVisible()
    for (let card of this.links.value) {
      if ( card.getText().includes(text) ) {
        card.click()
        break
      }
    }
  }


}

export default new HomePage()